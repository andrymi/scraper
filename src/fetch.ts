import fetch, { RequestInit, Response } from 'node-fetch'

import * as fs from 'fs'
import * as path from 'path'

if (!process.env.npm_package_version) {
  try {
    fs.statSync(path.join(__dirname, 'package.json'))
    process.env.npm_package_version = require('./package.json').version
  } catch (err) {
    process.env.npm_package_version = require('../package.json').version
  }
}
const VERSION = process.env.npm_package_version || 'unknown'
const USER_AGENT = `trichter-scraper/${VERSION} (+https://gitlab.com/trichter/scraper)`

export default async function fetchWrapped (url: string, o?: RequestInit): Promise<Response> {
  if (!o) o = {}
  if (!o.headers) o.headers = {}
  o.headers['User-Agent'] = USER_AGENT
  return fetch(url, o)
}
