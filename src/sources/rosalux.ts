import fetch from '../fetch'
import * as moment from 'moment'
import * as cheerio from 'cheerio'
import * as ical from 'ical'
import * as showdown from 'showdown'
import * as jsdom from 'jsdom'

import { ScrapeFunction, ScrapedEvent } from '../index'
import Logger from '@trichter/logger'

const converter = new showdown.Converter()
const dom = new jsdom.JSDOM()
const log = Logger('trichter-scraper/rosalux')

async function icalParseURL (url: string, options: any): Promise<any> {
  return new Promise((resolve, reject) => ical.fromURL(url, options, (err, data) => {
    if (err) reject(err)
    else resolve(data)
  }))
}

async function getIds (city: string, year: number, month: number): Promise<string[]> {
  const ids = []
  const res = await fetch(`https://www.rosalux.de/veranstaltungen/?tx_cbelasticsearch_searchform[year]=${year}&tx_cbelasticsearch_searchform[month]=${month}`)
  const body = await res.text()
  const $ = cheerio.load(body)

  $('.elasticsearch__result').each((index, el) => {
    const detail = $('.teaser__date-group--right', el)
    if (detail.text().indexOf(city) === -1) return
    const link = $('a.teaser__liner', el)[0].attribs.href
    const id = link.match(/es_detail\/([A-Z0-9]+)\//)
    if(!id) return
    ids.push(id[1])
  })
  return ids
}

const crawl: ScrapeFunction = async (options: {city: string, eventUrl?: string}) => {
  log.debug('load events')
  const now = moment()
  const nextMonth = moment().add(1, 'month')
  const ids = [].concat(
    await getIds(options.city, now.year(), now.month() + 1),
    await getIds(options.city, nextMonth.year(), nextMonth.month() + 1)
  )
  const events = []
  for (const id of ids) {
    log.debug(`get details for ${id}`)
    const ical = await icalParseURL(`https://www.rosalux.de/veranstaltung/ical/${id}/event.ics`, {})
    const e = ical[Object.keys(ical)[0]]
    if (!e.location) continue

    const res = await fetch(`https://www.rosalux.de/veranstaltung/es_detail/${id}/`)
    const $ = cheerio.load(await res.text())
    const descriptionHtml = $('.textmedia__text').html()

    const event: ScrapedEvent = {
      id: id,
      link: options.eventUrl ? options.eventUrl.replace(/:id/, id) : `https://www.rosalux.de/veranstaltung/es_detail/${id}/`,
      title: e.summary,
      teaser: e.description || null,
      start: moment(e.start).toISOString(),
      end: e.end ? moment(e.end).toISOString() : null,
      address: e.location,
      description: converter.makeMarkdown(descriptionHtml, dom.window.document).replace(/<br>/g, '')
    }

    if (e.location.match(/,/g).length >= 2) {
      event.locationName = e.location.split(', ')[0]
      event.address = e.location.split(', ').slice(1).join(', ')
    }
    events.push(event)
  }

  return events
}

export default crawl
