import * as moment from 'moment'
import { ScrapeFunction, ScrapedEvent } from '../index'
import Logger from '@trichter/logger'
import fetch from '../fetch'
import * as cheerio from 'cheerio'
import { URL } from 'url'

const log = Logger('trichter-scraper/bund')

const crawl: ScrapeFunction = async (options: {base: string, categoryIds: number[]}) => {
  moment.locale('de')

  try {
    const events: ScrapedEvent[] = []
    const links: string[] = []
    for (const categoryId of options.categoryIds) {
      const url = `${options.base}/?tx_bundpoolevent_display[filter][eventcategory]=${categoryId}`
      const res = await fetch(url)
      const $ = cheerio.load(await res.text())
      $('article.m-content-dashboardbox a').each((i, el) => {
        links.push(el.attribs.href)
      })
    }
    const u = new URL(options.base)
    const host = `${u.protocol}//${u.host}`
    for (const link of links) {
      log.debug('scrape ' + link)
      const res = await fetch(host + link)
      const $ = cheerio.load(await res.text())
      const title = $('meta[property="og:title"]')

      const imagePath = $('img.rte-image--image').attr('src')
      $('.rte-image__imageLeft').remove()
      $('.rte-paragraph__clearfix').remove()
      const description = $('.m-layout-2col--content__mainContent')
      const labels = $('.m-sidebar-content .rte-paragraph__nomargin')
      const sidebar: {[k: string]: string} = {}
      for (const label of labels.toArray()) {
        const key = $(label).text().slice(0, -1)
        const val = $(label).next().text().trim()
        // console.log({key,val})
        sidebar[key] = val
        // console.log(label)
      }
      if (!sidebar.Startdatum || !sidebar.Uhrzeit) {
        // didn't find enough information
        continue
      }
      const id = link.split('/')[5]
      const [startTime, , endTime] = sidebar.Uhrzeit.split(/(-|–)/).map(t => {
        t = t.trim().replace(/ Uhr/, '')
        if (t.match(/^\d+$/)) t += ':00'
        return t
      })

      const e: ScrapedEvent = {
        id: id,
        link: host + link,
        title: title.attr('content'),
        start: moment(`${sidebar.Startdatum}, ${startTime}`, 'DD. MMMM YYYY, HH:mm').toISOString(),
        end: endTime && sidebar.Enddatum ? moment(`${sidebar.Enddatum}, ${endTime}`, 'DD. MMMM YYYY, HH:mm').toISOString() : null,
        address: sidebar.Ort ? sidebar.Ort.replace(/^Leipzig, /, '').replace(/\s+/g, ' ') : null,
        description: description.text().trim()
      }
      if (imagePath) {
        try {
          const p = imagePath.split('/')
          const filename = p[p.length - 1]
          const res = await fetch(host + imagePath)
          const buf = await res.buffer()
          e.image = filename
          e.imageData = buf.toString('base64')
        } catch (err) {
          log.error(`could not get image ${imagePath} for event ${link}`, err)
        }
      }
      events.push(e)
    }

    return events
  } catch (err) {
    log.error(err)
    return []
  }
}
export default crawl
